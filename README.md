## OIDC Flask example

There are two routes:

- /: unauthenticated, you get a message to go to some other route
- /secure: this route is behind the token verification, you should pass it a 
valid token for your client ID.

### Running

```bash
pip install -r requirements.txt
python run.py
```

### Testing

Get the token by logging in to your API, then send it here using, for ex. this curl command:

```bash
curl -X GET "http://localhost:8000/secure" -H "Authorization: Bearer <token>"
```

The answer should be:

```json
{
  "ok": true
}
```