from flask import Flask, jsonify

from oidc_example.auth import oidc_validate


def create_app():
    """
    Application factory function
    :return:
    """
    app = Flask(__name__)
    app.config.from_object("oidc_example.config")
    return app


application = create_app()


@application.route("/secure")
@oidc_validate
def secure_route():
    return jsonify({"ok": True})


@application.route("/")
def index():
    return jsonify({"message": "Try navigating to '/secure' with / without "})
