DEBUG = True

CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"


# OIDC configuration
OIDC_CLIENT_ID = "bogus-rest-resource-management"
OIDC_JWKS_URL = "https://keycloak-dev.cern.ch/auth/realms/cern/protocol/openid-connect/certs"
OIDC_ISSUER = "https://keycloak-dev.cern.ch/auth/realms/cern"